# platform-engineer-tech

## Intro

This project contains solutions for the Ringier SA technical assessment questions.

1. RESTful Web Api
2. Terraform + AWS
3. Dockerize a JS application
