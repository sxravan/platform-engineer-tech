# docker-node-challenge

I have little to no experience with NodeJS, but I have used Dockerfile before, it was straight-forward

## How-to

For this question, I have followed the official documentation about dockerizing Node.js web app.

When I got a working one, I have implemented some Docker best practices example multi-stage build, deleting the cache, using tini (Tiny Init) to properly handle the container management.

As for the bonus part, to build the image with cache:

Make sure that the package `docker-buildx` is installed.

```bash
docker build --build-arg BUILDKIT_INLINE_CACHE=1 -t my-node-app .
```

The buildkit_inline_cache feature improve the performance on the build stage by reusing cached layers more efficiently.

The command is to be run in the same directory as the Dockerfile is.

To spool a container, use the following command:

```bash
docker run -p 8080:8080 -it my-node-app
```

