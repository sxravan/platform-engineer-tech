# python-flask-restful-api

I chose python since it is the only programming language that I have used in my professional career.

I have implemented a website using the Flask framework in 2019.

## How-to

For this question, I have prepared a virtualenv to isolate every package from my laptop.

```python
python -m venv MyApp
```

The code is pretty much straight forward.

The URI /system_info displays the information of the system at the time the script was launched.

os and shutil libraries have been used to provide those information.

The /get_return_value reads the tech_assess.json file and displays it on the browser / terminal.

The /set_return_value reads the tech_assess.json file, displays the values, and opens the file for writing purposes.

> The POST method should be used along with the data in json format.

Example

```bash
curl -H 'Content-Type: application/json' -XPOST -d '{ "return_value": 1503 }' http://localhost:8080/set_return_value
```

---

References:

1. https://flask.palletsprojects.com/en/2.3.x/quickstart/#routing
