# terraform-aws

While I have used terraform personally on my labs to create VMs on ProxmoxVE, I've never used it to control resources on Cloud (GCP, AWS)

Having no experiences in AWS, I managed to learn how it works by applying some GCP knowledge.

I have been able to complete the first part after ~ 10H of intensive reading and debugging.

## How-to

Make sure to install the following packages on your system:
* terraform
* aws (cli)

I have used a free tier on AmazonWS.

As for the region, I have used the `us-east-1`.

I have used the aws_instance resource to create a VM called Bastion, with instance type being t3.nano, choosing t3 since it is the latest one and giving more performance for the same price.

I have created my own VPC and did not use the default one and I wanted to understand the networking aspect on Clouds, this took most of the time.

```
resource "aws_instance" "bastion" {
  ami           = "ami-04cb4ca688797756f"
  instance_type = "t3.nano"
  subnet_id     = var.subnetid
  key_name      = var.keyname
  associate_public_ip_address = true

  tags = {
    Name = "bastion"
  }
}
```

The subnetid I used in the tfvars file correspond to one public interface, that is a public IP will be given to the machine on creation so that I can SSH to the server with the .pem file.

I created my own security group as well, it was auto-generated with the creation of the VPC.

The two rules needed to complete the challenge are found below:

```
resource "aws_vpc_security_group_ingress_rule" "bastion" {
  security_group_id = var.security_group
  cidr_ipv4   = "0.0.0.0/0"
  from_port   = 22
  ip_protocol = "tcp"
  to_port     = 22
}

resource "aws_vpc_security_group_egress_rule" "bastion" {
  security_group_id = var.security_group
  cidr_ipv4   = "0.0.0.0/0"
  from_port   = -1
  ip_protocol = "-1"
  to_port     = -1
}
```

The first one allows SSH connection the EC2 instance.

The second one, allows internet connection to the EC2 instance, it allows the instance to connect to the internet.

These are the variables I used to create the EC2 instance:

```
# Variables
variable "subnetid" {
  description = "Subnet ID"
}

variable "keyname" {
  description = "SSH Key Pair Name"
}

variable "vpc" {
  description = "VPC ID"
}

variable "security_group" {
  type = string
}
```

And for the tfvars file:

```
subnetid = "subnet-0d96ec5806d58c6cf"
keyname  = "shravan"
vpc = "vpc-047a19b7590c1a4c3"
security_group = "sg-0ef5fe8fb078088ac"
```

> NB I signed in to AWS using the aws-cli, I created an IAM user. This allows finer grain policies / privileges to the user.

SSH'ing to the newly created instance:

```
sxravan@Shravans-MacBook-Pro: ~/Downloads $ ssh -i shravan.pem ec2-user@3.85.106.191
The authenticity of host '3.85.106.191 (3.85.106.191)' can't be established.
ED25519 key fingerprint is SHA256:otFBQMFzTwyZpKzvf3voqYJd7yKD0GehweI+ssjw+Zs.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '3.85.106.191' (ED25519) to the list of known hosts.
   ,     #_
   ~\_  ####_        Amazon Linux 2023
  ~~  \_#####\
  ~~     \###|
  ~~       \#/ ___   https://aws.amazon.com/linux/amazon-linux-2023
   ~~       V~' '->
    ~~~         /
      ~~._.   _/
         _/ _/
       _/m/'
[ec2-user@ip-10-0-14-150 ~]$ w
 08:36:21 up 0 min,  1 user,  load average: 0.82, 0.27, 0.09
USER     TTY        LOGIN@   IDLE   JCPU   PCPU WHAT
ec2-user pts/0     08:36    3.00s  0.01s  0.00s w
```

I have executed `terraform destroy` after testing the terraform configurations.

---

BONUS Part

For the graviton, I used the interface help to find the corresponding AMIs.

I used t4g.nano instance type with ami-06f9c0b2ce386dda7.

The variables to deploy the Graviton instance is as follows:

# terraform init
# terraform plan --var-file=graviton.tfvars
# terraform apply --auto-approve

You can then SSH to the graviton instance.

The following output is from the Graviton instance:

```
sxravan@Shravans-MacBook-Pro: ~/Downloads $ ssh -i shravan.pem ec2-user@3.84.215.36
The authenticity of host '3.84.215.36 (3.84.215.36)' can't be established.
ED25519 key fingerprint is SHA256:W0QTp20GEDjXm5hhpo+3LDs5G1sjTMh6Gi8WwDNRBNI.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '3.84.215.36' (ED25519) to the list of known hosts.
   ,     #_
   ~\_  ####_        Amazon Linux 2023
  ~~  \_#####\
  ~~     \###|
  ~~       \#/ ___   https://aws.amazon.com/linux/amazon-linux-2023
   ~~       V~' '->
    ~~~         /
      ~~._.   _/
         _/ _/
       _/m/'
[ec2-user@ip-10-0-0-191 ~]$ cat /etc/os-release
NAME="Amazon Linux"
VERSION="2023"
ID="amzn"
ID_LIKE="fedora"
VERSION_ID="2023"
PLATFORM_ID="platform:al2023"
PRETTY_NAME="Amazon Linux 2023"
ANSI_COLOR="0;33"
CPE_NAME="cpe:2.3:o:amazon:amazon_linux:2023"
HOME_URL="https://aws.amazon.com/linux/"
BUG_REPORT_URL="https://github.com/amazonlinux/amazon-linux-2023"
SUPPORT_END="2028-03-01"
[ec2-user@ip-10-0-0-191 ~]$ uname -a
Linux ip-10-0-0-191.ec2.internal 6.1.49-70.116.amzn2023.x86_64 #1 SMP PREEMPT_DYNAMIC Wed Sep  6 22:13:07 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
[ec2-user@ip-10-0-0-191 ~]$ cat /proc/cpuinfo
processor	: 0
vendor_id	: GenuineIntel
cpu family	: 6
model		: 85
model name	: Intel(R) Xeon(R) Platinum 8259CL CPU @ 2.50GHz
stepping	: 7
microcode	: 0x5003604
cpu MHz		: 2500.004
cache size	: 36608 KB
physical id	: 0
siblings	: 2
core id		: 0
cpu cores	: 1
apicid		: 0
initial apicid	: 0
fpu		: yes
fpu_exception	: yes
cpuid level	: 13
wp		: yes
flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss ht syscall nx pdpe1gb rdtscp lm constant_tsc rep_good nopl xtopology nonstop_tsc cpuid tsc_known_freq pni pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm abm 3dnowprefetch invpcid_single pti fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx avx512f avx512dq rdseed adx smap clflushopt clwb avx512cd avx512bw avx512vl xsaveopt xsavec xgetbv1 xsaves ida arat pku ospke
bugs		: cpu_meltdown spectre_v1 spectre_v2 spec_store_bypass l1tf mds swapgs itlb_multihit mmio_stale_data retbleed gds
bogomips	: 5000.00
clflush size	: 64
cache_alignment	: 64
address sizes	: 46 bits physical, 48 bits virtual
power management:

processor	: 1
vendor_id	: GenuineIntel
cpu family	: 6
model		: 85
model name	: Intel(R) Xeon(R) Platinum 8259CL CPU @ 2.50GHz
stepping	: 7
microcode	: 0x5003604
cpu MHz		: 2500.004
cache size	: 36608 KB
physical id	: 0
siblings	: 2
core id		: 0
cpu cores	: 1
apicid		: 1
initial apicid	: 1
fpu		: yes
fpu_exception	: yes
cpuid level	: 13
wp		: yes
flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss ht syscall nx pdpe1gb rdtscp lm constant_tsc rep_good nopl xtopology nonstop_tsc cpuid tsc_known_freq pni pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm abm 3dnowprefetch invpcid_single pti fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx avx512f avx512dq rdseed adx smap clflushopt clwb avx512cd avx512bw avx512vl xsaveopt xsavec xgetbv1 xsaves ida arat pku ospke
bugs		: cpu_meltdown spectre_v1 spectre_v2 spec_store_bypass l1tf mds swapgs itlb_multihit mmio_stale_data retbleed gds
bogomips	: 5000.00
clflush size	: 64
cache_alignment	: 64
address sizes	: 46 bits physical, 48 bits virtual
power management:

[ec2-user@ip-10-0-0-191 ~]$ nproc
2
[ec2-user@ip-10-0-0-191 ~]$ free -h
               total        used        free      shared  buff/cache   available
Mem:           407Mi       159Mi        77Mi       0.0Ki       169Mi       236Mi
Swap:             0B          0B          0B
[ec2-user@ip-10-0-0-191 ~]$ lscpu
Architecture:            x86_64
  CPU op-mode(s):        32-bit, 64-bit
  Address sizes:         46 bits physical, 48 bits virtual
  Byte Order:            Little Endian
CPU(s):                  2
  On-line CPU(s) list:   0,1
Vendor ID:               GenuineIntel
  Model name:            Intel(R) Xeon(R) Platinum 8259CL CPU @ 2.50GHz
    CPU family:          6
    Model:               85
    Thread(s) per core:  2
    Core(s) per socket:  1
    Socket(s):           1
    Stepping:            7
    BogoMIPS:            5000.00
    Flags:               fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss ht syscall nx pdpe1gb rdtscp lm constant_tsc rep_good nopl xtopology nonstop_tsc cpuid tsc_known_freq pni pclmulqdq ssse3 fma cx1
                         6 pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm abm 3dnowprefetch invpcid_single pti fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx avx512f avx512dq rdseed adx sma
                         p clflushopt clwb avx512cd avx512bw avx512vl xsaveopt xsavec xgetbv1 xsaves ida arat pku ospke
Virtualization features:
  Hypervisor vendor:     KVM
  Virtualization type:   full
Caches (sum of all):
  L1d:                   32 KiB (1 instance)
  L1i:                   32 KiB (1 instance)
  L2:                    1 MiB (1 instance)
  L3:                    35.8 MiB (1 instance)
NUMA:
  NUMA node(s):          1
  NUMA node0 CPU(s):     0,1
Vulnerabilities:
  Gather data sampling:  Unknown: Dependent on hypervisor status
  Itlb multihit:         KVM: Mitigation: VMX unsupported
  L1tf:                  Mitigation; PTE Inversion
  Mds:                   Vulnerable: Clear CPU buffers attempted, no microcode; SMT Host state unknown
  Meltdown:              Mitigation; PTI
  Mmio stale data:       Vulnerable: Clear CPU buffers attempted, no microcode; SMT Host state unknown
  Retbleed:              Vulnerable
  Spec rstack overflow:  Not affected
  Spec store bypass:     Vulnerable
  Spectre v1:            Mitigation; usercopy/swapgs barriers and __user pointer sanitization
  Spectre v2:            Mitigation; Retpolines, STIBP disabled, RSB filling, PBRSB-eIBRS Not affected
  Srbds:                 Not affected
  Tsx async abort:       Not affected
```

---

References:

https://www.youtube.com/watch?v=hiKPPy584Mg
