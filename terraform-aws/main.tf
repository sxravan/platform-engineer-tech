terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region = "us-east-1"
} 

resource "aws_instance" "bastion" {
  ami           = var.ami
  instance_type = var.instance_type
  subnet_id     = var.subnetid
  key_name      = var.keyname
  associate_public_ip_address = true

  tags = {
    Name = "bastion"
  }
}

resource "aws_vpc_security_group_ingress_rule" "bastion" {
  security_group_id = var.security_group
  cidr_ipv4   = "0.0.0.0/0"
  from_port   = 22
  ip_protocol = "tcp"
  to_port     = 22
}

resource "aws_vpc_security_group_egress_rule" "bastion" {
  security_group_id = var.security_group
  cidr_ipv4   = "0.0.0.0/0"
  from_port   = -1
  ip_protocol = "-1"
  to_port     = -1
}

# Variables
variable "subnetid" {
  description = "Subnet ID"
}

variable "ami" {
  type = string
}

variable "keyname" {
  description = "SSH Key Pair Name"
}

variable "vpc" {
  description = "VPC ID"
}

variable "security_group" {
  type = string
}

variable "instance_type" {
  type = string
}
